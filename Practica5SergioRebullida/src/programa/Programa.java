package programa;

import java.io.IOException;
import java.util.Scanner;

import clases.Filmografia;
import clases.Genero;

public class Programa {

	static Scanner input = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		Filmografia filmes = new Filmografia();

		filmes.altaActor("Tom", "Hanks", 65, "Estadounidense", "California", "Tom Jeffrey Hanks", "Masculino", 10);
		filmes.altaActor("Tim", "Allen", 68, "Estadounidense", "Colorado", "Timothy Alan Dick", "Masculino", 2);
		filmes.altaActor("Charlize", "Theron", 46, "Estadounidense", "California", "Charlize Theron Maritz", "Femenino",
				8);
		filmes.altaActor("Matthew", "McConaughey", 52, "Estadounidense", "Austin", "Matthew David McConaughey",
				"Masculino", 9);
		filmes.altaActor("Jamie", "Foxx", 54, "Estadounidense", "Texas", "Eric Marlon Bishop", "Masculino", 5);
		filmes.altaActor("Leonardo", "DiCaprio", 47, "Estadounidense", "California", "Leonardo Wilhelm DiCaprio",
				"Masculino", 9);
		filmes.altaActor("Kate", "Winslet", 46, "Britanica", "Inglaterra", "Kate Elizabeth Winslet", "Femenino", 13);
		filmes.altaActor("Elliot", "Page", 35, "Canadiense", "Desconocido", "Ellen Grace Philpotts-Page", "Masculino",
				7);

		filmes.altaDirector("John", "Lasseter", 65, "Estadounidense", "California", "Toy Story", 5);
		filmes.altaDirector("Quentin", "Tarantino", 59, "Estadounidense", "Tennessee", "Pulp Fiction", 11);
		filmes.altaDirector("Christopher", "Nolan", 51, "Britanica", "California", "El Caballero Oscuro", 12);
		filmes.altaDirector("James", "Cameron", 67, "Canadiense", "Desconocido", "Avatar", 11);
		filmes.altaDirector("Travis", "Knight", 48, "Estadounidense", "Desconocido", "Bumblebee", 4);

		filmes.altaPeliculaAnimacion("Toy Story", Genero.AVENTURA, 1996, 81, "Ingles", "CGI");
		filmes.altaPeliculaAnimacion("Toy Story 2", Genero.AVENTURA, 1999, 93, "Ingles", "CGI");
		filmes.altaPeliculaAnimacion("Kubo y las dos cuerdas magicas", Genero.AVENTURA, 2016, 102, "Laika", "Ingles",
				"Stop motion");
		filmes.altaPeliculaComercial("Django Desencadenado", Genero.WESTERN, 4.0, 2012, 170, "Columbia Pictures", "+16",
				"425000000", "Freedom", "Django Freeman");
		filmes.altaPeliculaComercial("Titanic", Genero.ROMANCE, 4.3, 1997, 195, "20th Century Fox", "+12", "2100000000",
				"Hymn to the sea", "Jack Dawson");
		filmes.altaPeliculaComercial("Origen", Genero.CIENCIA_FICCION, 3.9, 2010, 148, "Warner Bros. Pictures", "+12",
				"825000000", "Inception", "Dom Cobb");
		filmes.altaPeliculaIndependiente("Reservoir Dogs", Genero.CRIMEN, 4.6, 1992, 99, "Live Entertainment", 1200000,
				null, false);
		filmes.altaPeliculaIndependiente("Memento", Genero.SUSPENSO, 4.3, 2001, 113, "Summit Entertainment", 9000000,
				"Memento mori", false);

		filmes.asignarActor1Pelicula("Tom", "Toy Story");
		filmes.asignarActor2Pelicula("Tim", "Toy Story");
		filmes.asignarDirectorPelicula("John", "Toy Story");
		filmes.asignarActor1Pelicula("Tom", "Toy Story 2");
		filmes.asignarActor2Pelicula("Tim", "Toy Story 2");
		filmes.asignarDirectorPelicula("John", "Toy Story 2");
		filmes.asignarActor1Pelicula("Charlize", "Kubo y las dos cuerdas magicas");
		filmes.asignarActor2Pelicula("Matthew", "Kubo y las dos cuerdas magicas");
		filmes.asignarDirectorPelicula("Travis", "Kubo y las dos cuerdas magicas");
		filmes.asignarActor1Pelicula("Jamie", "Django Desencadenado");
		filmes.asignarActor2Pelicula("Leonardo", "Django Desencadenado");
		filmes.asignarDirectorPelicula("Quentin", "Django Desencadenado");
		filmes.asignarActor1Pelicula("Leonardo", "Titanic");
		filmes.asignarActor2Pelicula("Kate", "Titanic");
		filmes.asignarDirectorPelicula("James", "Titanic");
		filmes.asignarActor1Pelicula("Leonardo", "Origen");
		filmes.asignarActor2Pelicula("Elliot", "Origen");
		filmes.asignarDirectorPelicula("Christopher", "Origen");
		filmes.asignarDirectorPelicula("Quentin", "Reservoir Dogs");
		filmes.asignarDirectorPelicula("Christopher", "Memento");

		int opcion;

		do {
			opcion = menu();
			switch (opcion) {
			case 1:
				int opcionBuscar;
				do {
					System.out.println("\n1. Buscar actor");
					System.out.println("2. Buscar director");
					System.out.println("3. Buscar pelicula");
					System.out.println("4. Salir al menu principal");
					opcionBuscar = input.nextInt();
					input.nextLine();

					switch (opcionBuscar) {
					case 1:
						System.out.println("Introduce el nombre del actor a buscar:");
						String nombreActor = input.nextLine();
						if (filmes.buscarActor(nombreActor) != null) {
							System.out.println(filmes.buscarActor(nombreActor));
						} else {
							System.out.println("No existe el actor " + nombreActor);
						}
						break;
					case 2:
						System.out.println("Introduce el nombre del director a buscar:");
						String nombreDirector = input.nextLine();
						if (filmes.buscarDirector(nombreDirector) != null) {
							System.out.println(filmes.buscarDirector(nombreDirector));
						} else {
							System.out.println("No existe el director " + nombreDirector);
						}
						break;
					case 3:
						System.out.println("Introduce el titulo de la pelicula:");
						String tituloPelicula = input.nextLine();
						if (filmes.buscarPelicula(tituloPelicula) != null) {
							System.out.println(filmes.buscarPelicula(tituloPelicula));
						} else {
							System.out.println("Sin informaci�n acerca de " + tituloPelicula);
						}
						break;
					case 4:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionBuscar != 4);
				break;

			case 2:
				int opcionEliminar;
				do {
					System.out.println("\n1. Eliminar actores");
					System.out.println("2. Eliminar directores");
					System.out.println("3. Eliminar peliculas");
					System.out.println("4. Salir al menu principal");
					opcionEliminar = input.nextInt();
					input.nextLine();

					switch (opcionEliminar) {
					case 1:
						System.out.println("Introduce el actor que quiera eliminar:");
						String nombreActor = input.nextLine();
						filmes.eliminarActor(nombreActor);
						break;
					case 2:
						System.out.println("Introduce el director que quiera eliminar:");
						String nombreDirector = input.nextLine();
						filmes.eliminarDirector(nombreDirector);
						break;
					case 3:
						System.out.println("Introduce la pelicula que quiera eliminar:");
						String tituloPelicula = input.nextLine();
						filmes.eliminarPelicula(tituloPelicula);
						break;
					case 4:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionEliminar != 4);
				break;

			case 3:
				int opcionListar;
				do {
					System.out.println("\n1. Listar actores");
					System.out.println("2. Listar directores");
					System.out.println("3. Listar peliculas");
					System.out.println("4. Salir al menu principal");
					opcionListar = input.nextInt();
					input.nextLine();

					switch (opcionListar) {
					case 1:
						filmes.listarActores();
						break;
					case 2:
						filmes.listarDirectores();
						break;
					case 3:
						filmes.listarPeliculas();
						break;
					case 4:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionListar != 4);
				break;

			case 4:
				int opcionListarPeli;
				do {
					System.out.println("\n1. Listar peliculas por actor");
					System.out.println("2. Listar peliculas por director");
					System.out.println("3. Salir al menu principal");
					opcionListarPeli = input.nextInt();
					input.nextLine();

					switch (opcionListarPeli) {
					case 1:
						System.out.println("Introduce el nombre del actor:");
						String nombreActor = input.nextLine();
						filmes.listarPeliculasDeActor(nombreActor);
						break;
					case 2:
						System.out.println("Introduce el nombre del director:");
						String nombreDirector = input.nextLine();
						filmes.listarPeliculasDeDirector(nombreDirector);
						break;
					case 3:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionListarPeli != 3);
				break;

			case 5:
				int opcionOrdenar;
				do {
					System.out.println("\n1. Ordenar peliculas por fecha de estreno (antigua)");
					System.out.println("2. Ordenar peliculas por fecha de estreno (reciente)");
					System.out.println("3. Salir al menu principal");
					opcionOrdenar = input.nextInt();
					input.nextLine();

					switch (opcionOrdenar) {
					case 1:
						filmes.ordenarPeliculasAnnoReciente();
						break;
					case 2:
						filmes.ordenarPeliculasAnnoAntigua();
						break;
					case 3:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionOrdenar != 3);
				break;

			case 6:
				int opcionFiltrar;
				do {
					System.out.println("\n1. Filtrar peliculas por genero");
					System.out.println("2. Filtrar peliculas por estudio de animacion");
					System.out.println("3. Salir al menu principal");
					opcionFiltrar = input.nextInt();
					input.nextLine();

					switch (opcionFiltrar) {
					case 1:
						filmes.peliculasGeneros();
						break;
					case 2:
						filmes.peliculasEstudios();
						break;
					case 3:
						System.out.println("SALIENDO...");
						break;
					default:
						System.out.println("No existe esa opcion");
						break;
					}
				} while (opcionFiltrar != 3);
				break;

			case 7:
				System.out.println("Introduce el titulo de la pelicula:");
				String titulo = input.nextLine();
				filmes.peliculasValoracion(titulo);
				break;

			case 8:
				System.out.println("�Que pelicula desea ver?");
				String tituloPelicula = input.nextLine();
				filmes.comprarEntradas(tituloPelicula, input);
				break;

			case 9:
				System.out.println("SISTEMA APAGADO");
				break;
			}
		} while (opcion != 9);

		input.close();

	}

	/**
	 * Metodo de creacion de un menu
	 * 
	 * @return opcion solicitada
	 */
	public static int menu() {
		int opcion = 0;
		boolean error = false;
		do {
			try {
				System.out.println("	    ________________ ");
				System.out.println("	  _|                |_ ");
				System.out.println("	 |   *   CINEMA   *   |");
				System.out.println("	 | |     TICKET     | |");
				System.out.println("	 |_  *  ADMIT ONE *  _|");
				System.out.println("	   |________________| ");

				System.out.println("\n------------------MENU------------------");
				System.out.println("1. Buscador");
				System.out.println("2. Eliminar");
				System.out.println("3. Listar");
				System.out.println("4. Listar peliculas por persona");
				System.out.println("5. Ordenar peliculas por fecha");
				System.out.println("6. Filtrar peliculas");
				System.out.println("7. Buscar peliculas y mostrar calificacion");
				System.out.println("8. Comprar entradas");
				System.out.println("9. Finalizar programa");
				System.out.println("----------------------------------------");
				System.out.print("Introduce una opcion: ");
				opcion = input.nextInt();
				input.nextLine();
				if (opcion < 1 || opcion > 9) {
					System.out.println("Opcion invalida");
					error = true;
				} else {
					error = false;
				}
			} catch (Exception e) {
				System.err.println("Error, debes introducir un numero");
				input.nextLine();
				error = true;
			}
		} while (error);
		return opcion;
	}

}
