package clases;

public class Director extends Persona {
	String peliculaMasExitosa;
	int nPeliculasRodadas;
	boolean retirado;

	/**
	 * Constructor vacio
	 */
	public Director() {

	}

	/**
	 * Constructor de Director con algunos atributos
	 * 
	 * @param nombre             nombre del director
	 * @param apellidos          apellidos del director
	 * @param edad               edad del director
	 * @param nacionalidad       nacionalidad del director
	 * @param peliculaMasExitosa titulo de la pelicula mas exitosa
	 * @param retirado           si se encuentra en activo o no
	 */
	public Director(String nombre, String apellidos, int edad, String nacionalidad, String peliculaMasExitosa,
			boolean retirado) {
		super(nombre, apellidos, edad, nacionalidad);
		this.peliculaMasExitosa = peliculaMasExitosa;
		this.retirado = retirado;
	}

	/**
	 * Constructor de Director con todos sus atributos
	 * 
	 * @param nombre             nombre del director
	 * @param apellidos          apellidos del director
	 * @param edad               edad del director
	 * @param nacionalidad       nacionalidad del director
	 * @param residencia         pais donde vive el director
	 * @param peliculaMasExitosa titulo de la pelicula mas exitosa
	 * @param nPeliculasRodadas  numero total de peliculas dirigidas
	 */
	public Director(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String peliculaMasExitosa, int nPeliculasRodadas) {
		super(nombre, apellidos, edad, nacionalidad, residencia);
		this.peliculaMasExitosa = peliculaMasExitosa;
		this.nPeliculasRodadas = nPeliculasRodadas;
		this.retirado = false;
	}

	/**
	 * Constructor de Director con todos sus atributos
	 * 
	 * @param nombre             nombre del director
	 * @param apellidos          apellidos del director
	 * @param edad               edad del director
	 * @param nacionalidad       nacionalidad del director
	 * @param residencia         pais donde vive el director
	 * @param peliculaMasExitosa titulo de la pelicula mas exitosa
	 * @param nPeliculasRodadas  numero total de peliculas dirigidas
	 * @param retirado           si se encuentra en activo o no
	 */
	public Director(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String peliculaMasExitosa, int nPeliculasRodadas, boolean retirado) {
		super(nombre, apellidos, edad, nacionalidad, residencia);
		this.peliculaMasExitosa = peliculaMasExitosa;
		this.nPeliculasRodadas = nPeliculasRodadas;
		this.retirado = retirado;
	}

	public String getPeliculaMasExitosa() {
		return peliculaMasExitosa;
	}

	public void setPeliculaMasExitosa(String peliculaMasExitosa) {
		this.peliculaMasExitosa = peliculaMasExitosa;
	}

	public int getnPeliculasRodadas() {
		return nPeliculasRodadas;
	}

	public void setnPeliculasRodadas(int nPeliculasRodadas) {
		this.nPeliculasRodadas = nPeliculasRodadas;
	}

	public boolean isRetirado() {
		return retirado;
	}

	public void setRetirado(boolean retirado) {
		this.retirado = retirado;
	}

	@Override
	public String toString() {
		if (retirado == true) {
			return super.toString() + ", Pelicula mas exitosa: " + peliculaMasExitosa + ", Numero peliculas: "
					+ nPeliculasRodadas + ", Actualmente retirado";
		} else {
			return super.toString() + ", Pelicula mas exitosa: " + peliculaMasExitosa + ", Numero peliculas: "
					+ nPeliculasRodadas + ", En activo";
		}
	}

	/**
	 * Metodo que muestra algunos atributos
	 * 
	 * @param director director que se quiere mostrar
	 */
	public static void mostrarDirector(Director director) {
		System.out.print(director.nombre + " " + director.apellidos + ", Edad: " + director.edad
				+ "\nPelicula mas exitosa: " + director.peliculaMasExitosa);
		System.out.println(director.retirado == true ? ", Actualmente retirado" : ", En activo");
	}
}
