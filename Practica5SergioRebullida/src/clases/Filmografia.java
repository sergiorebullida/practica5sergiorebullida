package clases;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Filmografia {
	private ArrayList<Actor> listaActores;
	private ArrayList<Director> listaDirectores;
	private ArrayList<Pelicula> listaPeliculas;

	/**
	 * Constructor para inicializar
	 */
	public Filmografia() {
		listaActores = new ArrayList<Actor>();
		listaDirectores = new ArrayList<Director>();
		listaPeliculas = new ArrayList<Pelicula>();
	}

	/**
	 * Metodo que da de alta actores
	 * 
	 * @param nombre           nombre del actor
	 * @param apellidos        apellidos del actor
	 * @param edad             edad del actor
	 * @param nacionalidad     nacionalidad del actor
	 * @param residencia       pais donde vive
	 * @param nombreNacimiento nombre de pila del actor
	 * @param genero           sexo del actor
	 * @param nPremios         numero de premios ganados
	 */
	public void altaActor(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String nombreNacimiento, String genero, int nPremios) {
		Actor nuevoCliente = new Actor(nombre, apellidos, edad, nacionalidad, residencia, nombreNacimiento, genero,
				nPremios);
		listaActores.add(nuevoCliente);
	}

	/**
	 * Metodo que da de alta directores en activo
	 * 
	 * @param nombre             nombre del director
	 * @param apellidos          apellidos del director
	 * @param edad               edad del director
	 * @param nacionalidad       nacionalidad del director
	 * @param residencia         pais donde vive
	 * @param peliculaMasExitosa titulo de la pelicula mas exitosa
	 * @param nPeliculasRodadas  numero total de peliculas dirigidas
	 */
	public void altaDirector(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String peliculaMasExitosa, int nPeliculasRodadas) {
		Director nuevoDirector = new Director(nombre, apellidos, edad, nacionalidad, residencia, peliculaMasExitosa,
				nPeliculasRodadas);
		listaDirectores.add(nuevoDirector);
	}

	/**
	 * Metodo que da de alta directores
	 * 
	 * @param nombre             nombre del director
	 * @param apellidos          apellidos del director
	 * @param edad               edad del director
	 * @param nacionalidad       nacionalidad del director
	 * @param residencia         pais donde vive
	 * @param peliculaMasExitosa titulo de la pelicula mas exitosa
	 * @param nPeliculasRodadas  numero total de peliculas dirigidas
	 * @param retirado           si se encuentra en activo o no
	 */
	public void altaDirector(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String peliculaMasExitosa, int nPeliculasRodadas, boolean retirado) {
		Director nuevoDirector = new Director(nombre, apellidos, edad, nacionalidad, residencia, peliculaMasExitosa,
				nPeliculasRodadas, retirado);
		listaDirectores.add(nuevoDirector);
	}

	/**
	 * Metodo que da de alta peliculas
	 * 
	 * @param titulo       nombre de la pelicula
	 * @param genero       genero de la pelicula
	 * @param calificacion valoracion sobre cinco
	 * @param annoEstreno  a�o en el que se estreno la pelicula
	 * @param duracion     duracion en minutos de la pelicula
	 * @param productora   nombre de la compa�ia productora
	 */
	public void altaPelicula(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion,
			String productora) {
		Pelicula nuevaPelicula = new Pelicula(titulo, genero, calificacion, annoEstreno, duracion, productora, 5.25);
		listaPeliculas.add(nuevaPelicula);
	}

	/**
	 * Metodo para dar de alta peliculas de animacion Pixar
	 * 
	 * @param titulo         nombre de la pelicula
	 * @param genero         genero de la pelicula
	 * @param annoEstreno    a�o en el que se estreno la pelicula
	 * @param duracion       duracion en minutos de la pelicula
	 * @param idiomaOriginal idioma original de la pelicula
	 * @param tecnica        tecnica de animacion
	 */
	public void altaPeliculaAnimacion(String titulo, Genero genero, int annoEstreno, int duracion,
			String idiomaOriginal, String tecnica) {
		Animacion nuevaPeliculaAnimacion = new Animacion(titulo, genero, annoEstreno, duracion, idiomaOriginal,
				tecnica);
		listaPeliculas.add(nuevaPeliculaAnimacion);
	}

	/**
	 * Metodo para dar de alta peliculas de animacion de un estudio diferente de
	 * Pixar
	 * 
	 * @param titulo           nombre de la pelicula
	 * @param genero           genero de la pelicula
	 * @param annoEstreno      a�o en el que se estreno la pelicula
	 * @param duracion         duracion en minutos de la pelicula
	 * @param estudioAnimacion nombre del estudio de animacion
	 * @param idiomaOriginal   idioma original de la pelicula
	 * @param tecnica          tecnica de animacion
	 */
	public void altaPeliculaAnimacion(String titulo, Genero genero, int annoEstreno, int duracion,
			String estudioAnimacion, String idiomaOriginal, String tecnica) {
		Animacion nuevaPeliculaAnimacion = new Animacion(titulo, genero, annoEstreno, duracion, estudioAnimacion,
				idiomaOriginal, tecnica);
		listaPeliculas.add(nuevaPeliculaAnimacion);
	}

	/**
	 * Metodo para dar de alta peliculas comerciales
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero de la pelicula
	 * @param calificacion  valoracion sobre cinco
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param productora    nombre de la compa�ia productora
	 * @param clasificacion clasificacion por edades
	 * @param recaudacion   dinero recaudado
	 * @param bso           nombre de la banda sonora original
	 * @param protagonista  nombre del protegonista de la historia
	 */
	public void altaPeliculaComercial(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion,
			String productora, String clasificacion, String recaudacion, String bso, String protagonista) {
		Comercial nuevaPeliculaComercial = new Comercial(titulo, genero, calificacion, annoEstreno, duracion,
				productora, clasificacion, recaudacion, bso, protagonista);
		listaPeliculas.add(nuevaPeliculaComercial);
	}

	/**
	 * Metodo para dar de alta peliculas independientes
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero de la pelicula
	 * @param calificacion  valoracion sobre cinco
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param productora    nombre de la compa�ia productora
	 * @param presupuesto   dinero utilizado para crear la pelicula
	 * @param basadaEn      basada en la obra original
	 * @param ganadoraOscar si ha sido ganadora de un Oscar
	 */
	public void altaPeliculaIndependiente(String titulo, Genero genero, double calificacion, int annoEstreno,
			int duracion, String productora, int presupuesto, String basadaEn, boolean ganadoraOscar) {
		Independiente nuevaPeliculaIndependiente = new Independiente(titulo, genero, calificacion, annoEstreno,
				duracion, productora, presupuesto, basadaEn, ganadoraOscar);
		listaPeliculas.add(nuevaPeliculaIndependiente);
	}

	/**
	 * Metodo para asignar a una pelicula un actor1
	 * 
	 * @param nombre nombre del actor1
	 * @param titulo nombre de la pelicula
	 */
	public void asignarActor1Pelicula(String nombre, String titulo) {
		Actor actor1 = buscarActor(nombre);
		Pelicula pelicula = buscarPelicula(titulo);
		pelicula.setActor1(actor1);
	}

	/**
	 * Metodo para asignar a una pelicula un actor2
	 * 
	 * @param nombre nombre del actor2
	 * @param titulo nombre de la pelicula
	 */
	public void asignarActor2Pelicula(String nombre, String titulo) {
		Actor actor2 = buscarActor(nombre);
		Pelicula pelicula = buscarPelicula(titulo);
		pelicula.setActor2(actor2);
	}

	/**
	 * Metodo para asignar a una pelicula un director
	 * 
	 * @param nombre nombre del director
	 * @param titulo nombre de la pelicula
	 */
	public void asignarDirectorPelicula(String nombre, String titulo) {
		Director director = buscarDirector(nombre);
		Pelicula pelicula = buscarPelicula(titulo);
		pelicula.setDirector(director);
	}

	/**
	 * Metodo para buscar un actor
	 * 
	 * @param nombre nombre del actor
	 * @return datos del actor
	 */
	public Actor buscarActor(String nombre) {
		for (Actor actor : listaActores) {
			if (actor != null && actor.getNombre().equalsIgnoreCase(nombre)) {
				return actor;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar un director
	 * 
	 * @param nombre nombre del director
	 * @return datos del director
	 */
	public Director buscarDirector(String nombre) {
		for (Director director : listaDirectores) {
			if (director != null && director.getNombre().equalsIgnoreCase(nombre)) {
				return director;
			}
		}
		return null;
	}

	/**
	 * Metodo para buscar una pelicula
	 * 
	 * @param titulo nombre de la pelicula
	 * @return datos de la pelicula
	 */
	public Pelicula buscarPelicula(String titulo) {
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula != null && pelicula.getTitulo().equalsIgnoreCase(titulo)) {
				return pelicula;
			}
		}
		return null;
	}

	/**
	 * Metodo para eliminar un actor
	 * 
	 * @param nombre nombre del actor
	 */
	public void eliminarActor(String nombre) {
		Iterator<Actor> iteradorActores = listaActores.iterator();
		while (iteradorActores.hasNext()) {
			Actor actor = iteradorActores.next();
			if (actor.getNombre().equalsIgnoreCase(nombre)) {
				iteradorActores.remove();
				System.err.println("��ACTOR ELIMINADO CORRECTAMENTE!!\n");
				listarActores();
			}
		}
	}

	/**
	 * Metodo para eliminar un director
	 * 
	 * @param nombre nombre del director
	 */
	public void eliminarDirector(String nombre) {
		Iterator<Director> iteradorDirectores = listaDirectores.iterator();
		while (iteradorDirectores.hasNext()) {
			Director director = iteradorDirectores.next();
			if (director.getNombre().equalsIgnoreCase(nombre)) {
				iteradorDirectores.remove();
				System.err.println("��DIRECTOR ELIMINADO CORRECTAMENTE!!\n");
				listarDirectores();
			}
		}
	}

	/**
	 * Metodo para eliminar una pelicula
	 * 
	 * @param titulo nombre de la pelicula
	 */
	public void eliminarPelicula(String titulo) {
		Iterator<Pelicula> iteradorPeliculas = listaPeliculas.iterator();
		while (iteradorPeliculas.hasNext()) {
			Pelicula pelicula = iteradorPeliculas.next();
			if (pelicula.getTitulo().equalsIgnoreCase(titulo)) {
				iteradorPeliculas.remove();
				System.err.println("��PELICULA ELIMINADO CORRECTAMENTE!!\n");
				listarPeliculas();
			}
		}
	}

	/**
	 * Metodo listar actores
	 */
	public void listarActores() {
		for (Actor actor : listaActores) {
			if (actor != null) {
				System.out.println(actor);
			}
		}
	}

	/**
	 * Metodo listar directores
	 */
	public void listarDirectores() {
		for (Director director : listaDirectores) {
			if (director != null) {
				System.out.println(director);
			}
		}
	}

	/**
	 * Metodo listar peliculas
	 */
	public void listarPeliculas() {
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula != null) {
				System.out.println(pelicula + "\n");
			}
		}
	}

	/**
	 * Metodo para listar las peliculas de un actor
	 * 
	 * @param nombre nombre del actor
	 */
	public void listarPeliculasDeActor(String nombre) {
		for (Pelicula pelicula : listaPeliculas) {
			if ((pelicula.getActor1() != null && pelicula.getActor1().getNombre().equalsIgnoreCase(nombre))
					|| (pelicula.getActor2() != null && pelicula.getActor2().getNombre().equalsIgnoreCase(nombre))) {
				System.out.println(pelicula.getTitulo() + ", " + pelicula.getAnnoEstreno());
			}
		}
	}

	/**
	 * Metodo para listar las peliculas de un director
	 * 
	 * @param nombre nombre del director
	 */
	public void listarPeliculasDeDirector(String nombre) {
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula.getDirector() != null && pelicula.getDirector().getNombre().equalsIgnoreCase(nombre)) {
				System.out.println(pelicula.getTitulo() + ", " + pelicula.getAnnoEstreno());
			}
		}
	}

	/**
	 * Metodo para ordenar peliculas de mas antigua a mas reciente
	 */
	public void ordenarPeliculasAnnoReciente() {
		for (int i = 0; i < listaPeliculas.size(); i++) {
			if (listaPeliculas.get(i) != null) {
				for (int j = i + 1; j < listaPeliculas.size(); j++) {
					if (listaPeliculas.get(j) != null) {
						if (listaPeliculas.get(j).getAnnoEstreno() < listaPeliculas.get(i).getAnnoEstreno()) {
							Pelicula aux = listaPeliculas.get(i);
							listaPeliculas.set(i, listaPeliculas.get(j));
							listaPeliculas.set(j, aux);
						}
					}
				}
			}
			System.out.println(listaPeliculas.get(i).getTitulo() + ", " + listaPeliculas.get(i).getAnnoEstreno());
		}
	}

	/**
	 * Metodo para ordenar peliculas de mas reciente a mas antigua
	 */
	public void ordenarPeliculasAnnoAntigua() {
		for (int i = 0; i < listaPeliculas.size(); i++) {
			if (listaPeliculas.get(i) != null) {
				for (int j = i + 1; j < listaPeliculas.size(); j++) {
					if (listaPeliculas.get(j) != null) {
						if (listaPeliculas.get(j).getAnnoEstreno() > listaPeliculas.get(i).getAnnoEstreno()) {
							Pelicula aux = listaPeliculas.get(i);
							listaPeliculas.set(i, listaPeliculas.get(j));
							listaPeliculas.set(j, aux);
						}
					}
				}
			}
			System.out.println(listaPeliculas.get(i).getTitulo() + ", " + listaPeliculas.get(i).getAnnoEstreno());
		}
	}

	/**
	 * Metodo para pasar la lista de peliculas al metodo de filtrar
	 */
	public void peliculasGeneros() {
		Pelicula.filtrarGeneros(listaPeliculas);
	}

	/**
	 * Metodo para pasar la lista de peliculas al metodo de filtrar
	 */
	public void peliculasEstudios() {
		Animacion.filtrarEstudios(listaPeliculas);
	}

	/**
	 * Metodo para mostrar la valoracion y peliculas de similar valoracion
	 * 
	 * @param titulo nombre de la pelicula
	 */
	public void peliculasValoracion(String titulo) {
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula != null && pelicula.getTitulo().equalsIgnoreCase(titulo)) {
				if (pelicula instanceof Animacion) {
					System.out.println(pelicula.getTitulo() + ", " + pelicula.getAnnoEstreno() + "\nGenero: "
							+ pelicula.getGenero() + "\nClasificacion: Para todos los publicos");
				} else {
					System.out.println(pelicula.getTitulo() + ", " + pelicula.getAnnoEstreno() + "\nGenero: "
							+ pelicula.getGenero() + "\nCalificacion: " + pelicula.valoracion(pelicula.getCalificacion()));
				}
				for (Pelicula pelicula1 : listaPeliculas) {
					if ((pelicula1 != null) && (!pelicula1.getTitulo().equalsIgnoreCase(titulo))
							&& (pelicula1.valoracion(pelicula1.getCalificacion())
									.equals(pelicula.valoracion(pelicula.getCalificacion())))) {
						System.out.println("\nNuestra recomendacion:\n" + pelicula1.getTitulo() + ", "
								+ pelicula1.getAnnoEstreno() + "\nGenero: " + pelicula1.getGenero());
						break;
					}
				}
			}
		}
	}

	/**
	 * Metodo para calcular precio de las entradas
	 * 
	 * @param titulo nombre de la pelicula
	 * @param input  para la lectura
	 */
	public void comprarEntradas(String titulo, Scanner input) {
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula != null && pelicula.getTitulo().equalsIgnoreCase(titulo)) {
				System.out.println("�Cuantas entradas vas a comprar?");
				int nEntradas = input.nextInt();
				System.out.println("El precio es de " + (nEntradas * pelicula.getPrecioEntrada()) + "� por " + nEntradas
						+ " entradas");
				System.out.println("��Coge palomitas y disfruta!!");
			}
		}
	}

}
