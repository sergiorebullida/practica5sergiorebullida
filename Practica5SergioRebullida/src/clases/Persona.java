package clases;

public class Persona {
	String nombre;
	String apellidos;
	int edad;
	String nacionalidad;
	String residencia;

	/**
	 * Constructor vacio
	 */
	public Persona() {

	}

	/**
	 * Constructor de Persona con algunos atributos
	 * 
	 * @param nombre       nombre de la persona
	 * @param apellidos    apellidos de la persona
	 * @param edad         edad de la persona
	 * @param nacionalidad nacionalidad de la persona
	 */
	public Persona(String nombre, String apellidos, int edad, String nacionalidad) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.nacionalidad = nacionalidad;
	}

	/**
	 * Constructor de Persona con todos sus atributos
	 * 
	 * @param nombre       nombre de la persona
	 * @param apellidos    apellidos de la persona
	 * @param edad         edad de la persona
	 * @param nacionalidad nacionalidad de la persona
	 * @param residencia   pais donde vive
	 */
	public Persona(String nombre, String apellidos, int edad, String nacionalidad, String residencia) {
		this.nombre = nombre;
		this.apellidos = apellidos;
		this.edad = edad;
		this.nacionalidad = nacionalidad;
		this.residencia = residencia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public int getEdad() {
		return edad;
	}

	public void setEdad(int edad) {
		this.edad = edad;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getResidencia() {
		return residencia;
	}

	public void setResidencia(String residencia) {
		this.residencia = residencia;
	}

	@Override
	public String toString() {
		return nombre + " " + apellidos + ", Edad: " + edad + ", Nacionalidad: " + nacionalidad + ", Residencia: "
				+ residencia;
	}
}
