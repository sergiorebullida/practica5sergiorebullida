package clases;

public class Independiente extends Pelicula {
	int presupuesto;
	String basadaEn;
	boolean ganadoraOscar;

	/**
	 * Constructor vacio
	 */
	public Independiente() {

	}

	/**
	 * Constructor de Pelicula Independiente con algunos atributos
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero cinematografico
	 * @param calificacion  valoracion sobre cinco
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param productora    nombre de la compa�ia productora
	 * @param presupuesto   dinero que se utilizo para la produccion
	 * @param ganadoraOscar si ha recibido un premio Oscar
	 */
	public Independiente(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion,
			String productora, int presupuesto, boolean ganadoraOscar) {
		super(titulo, genero, calificacion, annoEstreno, duracion, productora, 5.25);
		this.presupuesto = presupuesto;
		this.ganadoraOscar = ganadoraOscar;
	}

	/**
	 * Constructor de Pelicula Independiente con todos sus atributos
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero cinematografico
	 * @param calificacion  valoracion sobre cinco
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param productora    nombre de la compa�ia productora
	 * @param presupuesto   dinero que se utilizo para la produccion
	 * @param basadaEn      obra en la que ha sido basada
	 * @param ganadoraOscar si ha recibido un premio Oscar
	 */
	public Independiente(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion,
			String productora, int presupuesto, String basadaEn, boolean ganadoraOscar) {
		super(titulo, genero, calificacion, annoEstreno, duracion, productora, 5.25);
		this.presupuesto = presupuesto;
		this.basadaEn = basadaEn;
		this.ganadoraOscar = ganadoraOscar;
	}

	public int getPresupuesto() {
		return presupuesto;
	}

	public void setPresupuesto(int presupuesto) {
		this.presupuesto = presupuesto;
	}

	public String getBasadaEn() {
		return basadaEn;
	}

	public void setBasadaEn(String basadaEn) {
		this.basadaEn = basadaEn;
	}

	public boolean isGanadoraOscar() {
		return ganadoraOscar;
	}

	public void setGanadoraOscar(boolean ganadoraOscar) {
		this.ganadoraOscar = ganadoraOscar;
	}

	@Override
	public String toString() {
		if (basadaEn == null) {
			return super.toString() + "\nPresupuesto: " + presupuesto + ganadoraOscar(ganadoraOscar) + "\nDirector: "
					+ director.getNombre() + " " + director.getApellidos();

		} else {
			return super.toString() + "\nPresupuesto: " + presupuesto + "\nBasada en: " + basadaEn
					+ ganadoraOscar(ganadoraOscar) + "\nDirector: " + director.getNombre() + " "
					+ director.getApellidos();
		}
	}

	/**
	 * Metodo para devolver un mensaje
	 * 
	 * @param ganadoraOscar si ha ganado un premio Oscar
	 * @return mensaje segun si ha ganado un Oscar o no
	 */
	public String ganadoraOscar(boolean ganadoraOscar) {
		if (ganadoraOscar == true) {
			return "\nGanadora Oscar: Ha ganado al menos un Oscar";
		} else {
			return "\nGanadora Oscar: No ha ganado ningun Oscar";
		}
	}
}
