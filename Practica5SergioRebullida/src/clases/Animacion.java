package clases;

import java.util.ArrayList;

public class Animacion extends Pelicula {
	String estudioAnimacion;
	String idiomaOriginal;
	String tecnica;

	/**
	 * Constructor vacio
	 */
	public Animacion() {

	}

	/**
	 * Constructor de Pelicula de Animacion Pixar
	 * 
	 * @param titulo         nombre de la pelicula
	 * @param genero         genero cinematografico
	 * @param annoEstreno    a�o de estreno en cines
	 * @param duracion       duracion de la pelicula
	 * @param idiomaOriginal idioma original de la pelicula
	 * @param tecnica        tecnica de animacion
	 */
	public Animacion(String titulo, Genero genero, int annoEstreno, int duracion, String idiomaOriginal,
			String tecnica) {
		super(titulo, genero, annoEstreno, duracion, 5.25);
		this.estudioAnimacion = "Pixar";
		this.idiomaOriginal = idiomaOriginal;
		this.tecnica = tecnica;
	}

	/**
	 * Constructor de Pelicula de Animacion con todos los atributos
	 * 
	 * @param titulo           nombre de la pelicula
	 * @param genero           genero cinematografico
	 * @param annoEstreno      a�o de estreno en cines
	 * @param duracion         duracion de la pelicula
	 * @param estudioAnimacion compa�ia encargada de la animacion
	 * @param idiomaOriginal   idioma original de la pelicula
	 * @param tecnica          tecnica de animacion
	 */
	public Animacion(String titulo, Genero genero, int annoEstreno, int duracion, String estudioAnimacion,
			String idiomaOriginal, String tecnica) {
		super(titulo, genero, annoEstreno, duracion, 5.25);
		this.estudioAnimacion = estudioAnimacion;
		this.idiomaOriginal = idiomaOriginal;
		this.tecnica = tecnica;
	}

	public String getEstudioAnimacion() {
		return estudioAnimacion;
	}

	public void setEstudioAnimacion(String estudioAnimacion) {
		this.estudioAnimacion = estudioAnimacion;
	}

	public String getIdiomaOriginal() {
		return idiomaOriginal;
	}

	public void setIdiomaOriginal(String idiomaOriginal) {
		this.idiomaOriginal = idiomaOriginal;
	}

	public String getTecnica() {
		return tecnica;
	}

	public void setTecnica(String tecnica) {
		this.tecnica = tecnica;
	}

	@Override
	public String toString() {
		return titulo + ", " + annoEstreno + "\nGenero: " + genero + "\nDuracion: " + duracion + " min"
				+ "\nEstudio de animacion: " + estudioAnimacion + "\nIdioma: " + idiomaOriginal + "\nTecnica: "
				+ tecnica + "\nDirector: " + director.getNombre() + " " + director.getApellidos() + "\nActor1: "
				+ actor1.getNombre() + " " + actor1.getApellidos() + "\nActor2: " + actor2.getNombre() + " "
				+ actor2.getApellidos();
	}

	/**
	 * Metodo para listar peliculas por estudios de animacion
	 * 
	 * @param listaPeliculas vector con todas las peliculas dadas de altas
	 */
	public static void filtrarEstudios(ArrayList<Pelicula> listaPeliculas) {
		String anterior = "";
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula instanceof Animacion && (((Animacion) pelicula).getEstudioAnimacion() != anterior)) {
				System.out.println(((Animacion) pelicula).getEstudioAnimacion().toUpperCase());
				for (Pelicula pelicula1 : listaPeliculas) {
					if ((pelicula1 instanceof Animacion) && (((Animacion) pelicula).getEstudioAnimacion()
							.equals(((Animacion) pelicula1).getEstudioAnimacion()))) {
						System.out.println(pelicula1.getTitulo() + ", " + pelicula1.getAnnoEstreno());
						anterior = ((Animacion) pelicula1).getEstudioAnimacion();
					}
				}
			}
		}
	}
}
