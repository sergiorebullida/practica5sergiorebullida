package clases;

import java.util.ArrayList;

public class Pelicula {
	String titulo;
	Genero genero;
	double calificacion;
	int annoEstreno;
	int duracion;
	String productora;
	Actor actor1;
	Actor actor2;
	Director director;
	double precioEntrada;

	/**
	 * Constructor vacio
	 */
	public Pelicula() {

	}

	/**
	 * Constructor de Pelicula con algunos atributos
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero cinematografico
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param precioEntrada precio de las entradas
	 */
	public Pelicula(String titulo, Genero genero, int annoEstreno, int duracion, double precioEntrada) {
		this.titulo = titulo;
		this.genero = genero;
		this.annoEstreno = annoEstreno;
		this.duracion = duracion;
		this.precioEntrada = precioEntrada;
	}

	/**
	 * Constructor de Pelicula con todos sus atributos
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero cinematografico
	 * @param calificacion  valoracion sobre cinco
	 * @param annoEstreno   a�o en el que se estreno la pelicula
	 * @param duracion      duracion en minutos de la pelicula
	 * @param productora    nombre de la compa�ia productora
	 * @param precioEntrada precio de las entradas
	 */
	public Pelicula(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion, String productora,
			double precioEntrada) {
		this.titulo = titulo;
		this.genero = genero;
		this.calificacion = calificacion;
		this.annoEstreno = annoEstreno;
		this.duracion = duracion;
		this.productora = productora;
		this.precioEntrada = precioEntrada;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public Genero getGenero() {
		return genero;
	}

	public void setGenero(Genero genero) {
		this.genero = genero;
	}

	public double getCalificacion() {
		return calificacion;
	}

	public void setCalificacion(double calificacion) {
		this.calificacion = calificacion;
	}

	public int getAnnoEstreno() {
		return annoEstreno;
	}

	public void setAnnoEstreno(int annoEstreno) {
		this.annoEstreno = annoEstreno;
	}

	public int getDuracion() {
		return duracion;
	}

	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}

	public String getProductora() {
		return productora;
	}

	public void setProductora(String productora) {
		this.productora = productora;
	}

	public double getPrecioEntrada() {
		return precioEntrada;
	}

	public void setPrecioEntrada(double precioEntrada) {
		this.precioEntrada = precioEntrada;
	}

	public Actor getActor1() {
		return actor1;
	}

	public void setActor1(Actor actor1) {
		this.actor1 = actor1;
	}

	public Actor getActor2() {
		return actor2;
	}

	public void setActor2(Actor actor2) {
		this.actor2 = actor2;
	}

	public Director getDirector() {
		return director;
	}

	public void setDirector(Director director) {
		this.director = director;
	}

	@Override
	public String toString() {
		return titulo + ", " + annoEstreno + "\nGenero: " + genero + "\nCalificacion: " + calificacion + "\nDuracion: "
				+ duracion + " min" + "\nProductora: " + productora;
	}

	/**
	 * Metodo que muestra todos los atributos de una pelicula
	 * 
	 * @param pelicula pelicula que se quiere mostrar
	 */
	public static void mostrarPeli(Pelicula pelicula) {
		System.out.println(pelicula.toString() + "\nDirector: " + pelicula.director + "\nActor 1: " + pelicula.actor1
				+ "\nActor 2: " + pelicula.actor2 + "\n");
	}

	/**
	 * Metodo para listar peliculas por genero cinematografico
	 * 
	 * @param listaPeliculas vector con todas las peliculas dadas de altas
	 */
	public static void filtrarGeneros(ArrayList<Pelicula> listaPeliculas) {
		Genero anterior = null;
		for (Pelicula pelicula : listaPeliculas) {
			if (pelicula.getGenero() != anterior) {
				System.out.println(pelicula.getGenero());
				for (Pelicula pelicula1 : listaPeliculas) {
					if (pelicula.getGenero().equals(pelicula1.getGenero())) {
						System.out.println(pelicula1.getTitulo() + ", " + pelicula1.getAnnoEstreno());
						anterior = pelicula1.getGenero();
					}
				}
			}
		}
	}

	/**
	 * Metodo que me dice la valoracion segun su calificacion
	 * 
	 * @param calificacion valoracion sobre cinco
	 * @return mensaje segun su calificacion
	 */
	public String valoracion(double calificacion) {
		if (calificacion > 4.5) {
			return "Muy buena";
		} else if (calificacion > 4.0) {
			return "Buena";
		} else if (calificacion > 3.0) {
			return "Regular";
		} else {
			return "Mala";
		}
	}
}
