package clases;

public class Actor extends Persona {
	String nombreNacimiento;
	String genero;
	int nPremios;

	/**
	 * Constructor vacio
	 */
	public Actor() {

	}

	/**
	 * Constructor de Actor con algunos atributos
	 * 
	 * @param nombre       nombre del actor
	 * @param apellidos    apellidos del actor
	 * @param edad         edad del actor
	 * @param nacionalidad nacionalidad del actor
	 * @param residencia   pais donde vive el actor
	 * @param genero       sexo del actor
	 * @param nPremios     numero de premios ganados
	 */
	public Actor(String nombre, String apellidos, int edad, String nacionalidad, String residencia, String genero,
			int nPremios) {
		super(nombre, apellidos, edad, nacionalidad, residencia);
		this.genero = genero;
		this.nPremios = nPremios;
	}

	/**
	 * Constructor de Actor con todos sus atributos
	 * 
	 * @param nombre           nombre del actor
	 * @param apellidos        apellidos del actor
	 * @param edad             edad del actor
	 * @param nacionalidad     nacionalidad del actor
	 * @param residencia       pais donde vive el actor
	 * @param nombreNacimiento nombre de pila del actor
	 * @param genero           sexo del actor
	 * @param nPremios         numero de premios ganados
	 */
	public Actor(String nombre, String apellidos, int edad, String nacionalidad, String residencia,
			String nombreNacimiento, String genero, int nPremios) {
		super(nombre, apellidos, edad, nacionalidad, residencia);
		this.nombreNacimiento = nombreNacimiento;
		this.genero = genero;
		this.nPremios = nPremios;
	}

	public String getNombreNacimiento() {
		return nombreNacimiento;
	}

	public void setNombreNacimiento(String nombreNacimiento) {
		this.nombreNacimiento = nombreNacimiento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public int getnPremios() {
		return nPremios;
	}

	public void setnPremios(int nPremios) {
		this.nPremios = nPremios;
	}

	@Override
	public String toString() {
		return nombre + " " + apellidos + ", Edad: " + edad + ", Genero: " + genero + ", Nacionalidad: " + nacionalidad
				+ ", Residencia: " + residencia + ", Numero de Premios: " + nPremios;
	}
}
