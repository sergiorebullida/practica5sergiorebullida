package clases;

public class Comercial extends Pelicula {
	String clasificacion;
	String recaudacion;
	String bso;
	String protagonista;

	/**
	 * Constructor vacio
	 */
	public Comercial() {

	}

	/**
	 * Constructor de Pelicula Comercial con todos los Atributos
	 * 
	 * @param titulo        nombre de la pelicula
	 * @param genero        genero cinematografico
	 * @param calificacion  puntuacion obtenida
	 * @param annoEstreno   a�o del estreno en cines
	 * @param duracion      duracion de la pelicula
	 * @param productora    compa�ia encargada de la produccion
	 * @param clasificacion clasificacion por edades
	 * @param recaudacion   dinero recaudado con la pelicula
	 * @param bso           nombre de la banda sonora original de la pelicula
	 * @param protagonista  nombre del protegonista
	 */
	public Comercial(String titulo, Genero genero, double calificacion, int annoEstreno, int duracion,
			String productora, String clasificacion, String recaudacion, String bso, String protagonista) {
		super(titulo, genero, calificacion, annoEstreno, duracion, productora, 8.00);
		this.clasificacion = clasificacion;
		this.recaudacion = recaudacion;
		this.bso = bso;
		this.protagonista = protagonista;
	}

	public String getClasificacion() {
		return clasificacion;
	}

	public void setClasificacion(String clasificacion) {
		this.clasificacion = clasificacion;
	}

	public String getRecaudacion() {
		return recaudacion;
	}

	public void setRecaudacion(String recaudacion) {
		this.recaudacion = recaudacion;
	}

	public String getBso() {
		return bso;
	}

	public void setBso(String bso) {
		this.bso = bso;
	}

	public String getProtagonista() {
		return protagonista;
	}

	public void setProtagonista(String protagonista) {
		this.protagonista = protagonista;
	}

	@Override
	public String toString() {
		return super.toString() + "\nClasificacion: " + clasificacion + "\nRecaudacion: " + separarMiles(recaudacion)
				+ "\nBSO: " + bso + "\nDirector: " + director.getNombre() + " " + director.getApellidos()
				+ "\nProtagonista: " + actor1.getNombre() + " " + actor1.getApellidos() + " (" + protagonista + ")"
				+ "\nActor2: " + actor2.getNombre() + " " + actor2.getApellidos();
	}

	/**
	 * Metodo para colocar los puntos en los miles
	 * 
	 * @param recaudacion cantidad recaudada con la pelicula
	 * @return recaudacion con los puntos
	 */
	public String separarMiles(String recaudacion) {
		StringBuilder recaudacionAux = new StringBuilder(recaudacion);
		recaudacionAux.reverse();
		int posicion = 3;
		while (posicion < recaudacionAux.length()) {
			recaudacionAux.insert(posicion, '.');
			posicion += 4;
		}
		recaudacionAux.reverse();
		return recaudacionAux.toString();
	}
}
